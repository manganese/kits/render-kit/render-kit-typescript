// Interfaces
import UnityColor from '@manganese/serialization-kit/interfaces/UnityColor';
import UnityQuaternion from '@manganese/serialization-kit/interfaces/UnityQuaternion';

export default interface DirectionalLightParameters {
  color: UnityColor;
  intensity: number;
  rotation: UnityQuaternion;
}
