// Interfaces
import Link from '@manganese/core-kit/interfaces/Link';

export default interface RenderSet {
  rendered: Link;
  diffuse: Link;
  normal: Link;
  depth: Link;
}
