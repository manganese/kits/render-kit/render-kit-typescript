// Interfaces
import UnityColor from '@manganese/serialization-kit/interfaces/UnityColor';
import UnityVector3 from '@manganese/serialization-kit/interfaces/UnityVector3';

export default interface PointLightParameters {
  color: UnityColor;
  intensity: number;
  relativePosition: UnityVector3;
}
