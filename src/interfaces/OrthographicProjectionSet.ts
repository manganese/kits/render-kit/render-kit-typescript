// Interfaces
import Link from '@manganese/core-kit/interfaces/Link';

export default interface OrthographicProjectionSet {
  north: Link;
  east: Link;
  south: Link;
  west: Link;
}
