// Interfaces
import Link from '@manganese/core-kit/interfaces/Link';

export default interface Render {
  rendered: Link;
}
