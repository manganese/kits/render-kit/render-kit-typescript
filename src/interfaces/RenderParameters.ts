// Interfaces
import UnityVector3 from '@manganese/serialization-kit/interfaces/UnityVector3';
import UnityQuaternion from '@manganese/serialization-kit/interfaces/UnityQuaternion';
import UnityColor from '@manganese/serialization-kit/interfaces/UnityColor';

import DirectionalLightParameters from './DirectionalLightParameters';
import PointLightParameters from './PointLightParameters';

export default interface RenderParameters {
  // Camera transform
  relativeCameraPosition: UnityVector3;
  relativeCameraRotation: UnityQuaternion;

  // Ambienbt lighting
  ambientSkyColor: UnityColor;
  ambientEquatorColor: UnityColor;
  ambientGroundColor: UnityColor;

  // Directional lights
  directionalLights: DirectionalLightParameters[];

  // Point lights
  pointLights: PointLightParameters[];
}
